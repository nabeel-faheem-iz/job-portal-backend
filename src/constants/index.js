const userTypes = {
  EMPLOYER: "EMPLOYER",
  JOBSEEKER: "JOBSEEKER",
};

const responseMessages = {
  SERVER_ERROR_MESSAGE: "Something went wrong on server side 🥲",
  DATA_CREATION_MESSAGE: "Data created successfully!",
  DATA_UPDATION_MESSAGE: "Data updated successfully!",
  DATA_DELETION_MESSAGE: "Data deleted successfully!",
  DATA_NON_EXISTENCE_MESSAGE: "Data doesn't exist",
};

module.exports = {
  userTypes,
  responseMessages,
};
