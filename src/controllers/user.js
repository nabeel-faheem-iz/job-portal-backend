const { Op } = require("sequelize");
require("dotenv").config();

const { User } = require("../db/models");
const { hashPassword } = require("../helpers");

const {
  userTypes: { EMPLOYER, JOBSEEKER },
  responseMessages: {
    DATA_CREATION_MESSAGE,
    DATA_UPDATION_MESSAGE,
    DATA_DELETION_MESSAGE,
    SERVER_ERROR_MESSAGE,
  },
} = require("../constants");

const createUser = async (req, res) => {
  try {
    // destructure request body
    const { firstName, lastName, phone, email, password, type } = req.body;
    const resume = type === JOBSEEKER && req.file ? req.file.filename : null;

    // check user with same email
    const userWithSameEmail = await User.findOne({
      where: {
        email,
      },
    });

    if (userWithSameEmail) {
      return res.status(400).json({
        status: false,
        message: "User with the same email already exists!",
      });
    }

    // check user with same phone
    const userWithSamePhone = await User.findOne({
      where: {
        phone,
      },
    });
    if (userWithSamePhone) {
      return res.status(400).json({
        status: false,
        message: "User with the same phone already exists!",
      });
    }

    // hash password
    const passwordHash = hashPassword(password);

    // create user
    const data = await User.create({
      firstName,
      lastName,
      phone,
      email,
      password: passwordHash,
      type,
      resume,
    });

    // remove data from result data
    delete data.dataValues.password;

    // success
    return res.status(201).json({
      status: true,
      message: DATA_CREATION_MESSAGE,
      data,
    });
  } catch (error) {
    // error
    console.log("Error while creating the user: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

const updateUser = async (req, res) => {
  try {
    // destructure required data
    const { id } = req.params;
    const userInformation = { ...req.body };

    // compare userId(id inside token) with id(parameter id) to avoid irrelevant account/profile updation

    // check user exists
    const user = await User.findOne({
      where: {
        id,
      },
    });

    if (!user) {
      return res.status(400).json({
        status: false,
        message: "User does not exist!",
      });
    }

    // check user with same phone
    if (userInformation.phone) {
      const userWithSamePhone = await User.findOne({
        where: {
          [Op.and]: [
            {
              phone: req.body.phone,
            },
            {
              id: {
                [Op.ne]: id,
              },
            },
          ],
        },
      });
      if (userWithSamePhone) {
        return res.status(400).json({
          status: false,
          message: "User with the same phone already exists!",
        });
      }
    }

    // hash password
    if (userInformation.password) {
      userInformation.password = hashPassword(userInformation.password);
    }

    if (req.file && userInformation === JOBSEEKER) {
      userInformation.resume = req.file.filename;
    }

    // create user
    await User.update(userInformation, {
      where: {
        id,
      },
    });

    const data = await User.findOne({
      where: {
        id,
      },
    });

    // remove data from result data
    delete data.dataValues.password;

    // success
    return res.json({
      status: true,
      message: DATA_UPDATION_MESSAGE,
      data,
    });
  } catch (error) {
    // error
    console.log("Error while updating the user: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

const getUsersByType = async (req, res) => {
  try {
    // desctructure parameters
    const { type } = req.query;

    // query users based on filter
    const queryFilter = {};
    if (type !== "" && type === "employers") {
      queryFilter.type = EMPLOYER;
    } else if (type !== "" && type === "jobseekers") {
      queryFilter.type = JOBSEEKER;
    }

    const data = await User.findAll({
      where: queryFilter,
      attributes: {
        exclude: ["password"],
      },
    });

    // success
    return res.status(200).json({
      status: true,
      data,
    });
  } catch (error) {
    // error
    console.log("Error while searching the users: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

const getUser = async (req, res) => {
  try {
    const { id } = req.params;

    const data = await User.findOne({
      where: {
        id,
      },
      attributes: {
        exclude: ["password"],
      },
    });

    // delete data.dataValues.password;

    return res.status(200).json({
      status: true,
      data,
    });
  } catch (error) {
    console.log("Error while fetching the user: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

const deleteUser = async (req, res) => {
  try {
    // destructure required data
    const { id } = req.params;

    // find user with the id
    const user = await User.findOne({
      where: {
        id,
      },
    });

    if (!user) {
      return res.status(400).send({
        status: false,
        message: "User doesn't exist!",
      });
    }

    // delete the user
    await User.destroy({
      where: {
        id,
      },
    });
    // await User.destroy({
    //   where: {
    //     [Op.and]: [
    //       {
    //         id,
    //       },
    //       {
    //         id: {
    //           [Op.ne]: userId,
    //         },
    //       },
    //     ],
    //   },
    // });

    // success
    return res.json({
      status: true,
      message: DATA_DELETION_MESSAGE,
    });
  } catch (error) {
    console.log("Error while fetching the user: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

module.exports = {
  createUser,
  updateUser,
  getUsersByType,
  getUser,
  deleteUser,
};
