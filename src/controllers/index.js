const {
  createUser,
  updateUser,
  getUsersByType,
  getUser,
  deleteUser,
} = require("./user");
const { login } = require("./auth");
const {
  createJob,
  updateJob,
  getJobs,
  getJob,
  applyJob,
  deleteJob,
} = require("./job");
const {
  scheduleInterview,
  getUserSpecificInterviews,
  rescheduleInterview,
  deleteInterview,
} = require("./interview");
const { getCategories } = require("./category");

module.exports = {
  login,
  createUser,
  updateUser,
  getUsersByType,
  getUser,
  deleteUser,
  createJob,
  updateJob,
  getJobs,
  getJob,
  applyJob,
  deleteJob,
  scheduleInterview,
  getUserSpecificInterviews,
  rescheduleInterview,
  deleteInterview,
  getCategories,
};
