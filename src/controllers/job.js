const { Op } = require("sequelize");

const { Job, UserJob, User, sequelize } = require("../db/models");
const {
  userTypes: { JOBSEEKER, EMPLOYER },
  responseMessages: {
    DATA_CREATION_MESSAGE,
    DATA_UPDATION_MESSAGE,
    DATA_DELETION_MESSAGE,
    DATA_NON_EXISTENCE_MESSAGE,
    SERVER_ERROR_MESSAGE,
  },
} = require("../constants");

const createJob = async (req, res) => {
  try {
    // destructure required data
    const { id, type } = req.user;
    const { name, positions, lastDate } = req.body;

    // check if user type is "JOBSEEKER"
    if (type === JOBSEEKER) {
      return res.status(400).json({
        status: false,
        message: "JobSeeker cannot create job!",
      });
    }

    // create the job
    const result = await sequelize.transaction(async (t) => {
      const data = await Job.create(
        {
          name,
          positions,
          lastDate,
        },
        {
          transaction: t,
        }
      );

      await UserJob.create(
        {
          jobId: data.id,
          userId: id,
        },
        {
          transaction: t,
        }
      );

      return data;
    });

    // success
    return res.json({
      status: true,
      message: DATA_CREATION_MESSAGE,
      data: result,
    });
  } catch (error) {
    // error
    console.log("Error while creating job: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

const updateJob = async (req, res) => {
  try {
    // destructure required data
    const { id: userId } = req.user;
    const { id } = req.params;

    // check if job exists for the user
    const userWithJob = await User.findOne({
      where: {
        id: userId,
        type: EMPLOYER,
      },
      attributes: {
        include: [],
      },
      include: {
        model: Job,
        as: "userJobs",
        where: {
          id,
        },
      },
    });

    if (!userWithJob || !userWithJob.userJobs.length) {
      return res.status(400).json({
        status: true,
        message: "Employer doesn't own this job!",
      });
    }

    // update the job
    await Job.update(
      { ...req.body },
      {
        where: {
          id,
        },
      }
    );
    const data = await Job.findOne({
      where: {
        id,
      },
    });

    // success
    return res.json({
      status: true,
      message: DATA_UPDATION_MESSAGE,
      data,
    });
  } catch (error) {
    // error
    console.log("Error while creating job: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

/**
 * TODO: Add category filter support as well
 */
const getJobs = async (req, res) => {
  try {
    // destructure required data
    const { name } = req.query;

    // query jobs based on filter
    const queryFilter = {};
    if (name) {
      queryFilter.where = {
        name: {
          [Op.like]: `%${name}%`,
        },
      };
    }
    // if (category) {

    // }

    const data = await Job.findAll(queryFilter);

    // success
    return res.json({
      status: true,
      data,
    });
  } catch (error) {
    // error
    console.log("Error while search jobs: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

const getJob = async (req, res) => {
  try {
    // destructure required data
    const { id } = req.params;

    // check if job exists
    const data = await Job.findOne({
      where: {
        id,
      },
    });
    if (!data) {
      return res.status(400).send({
        status: false,
        message: DATA_NON_EXISTENCE_MESSAGE,
      });
    }

    // success
    return res.json({
      status: true,
      data,
    });
  } catch (error) {
    // error
    console.log("Error while applying for the job: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

const applyJob = async (req, res) => {
  try {
    // destructure required data
    const { id: userId, type } = req.user;
    const { jobId } = req.body;

    // check if user is employer
    if (type === EMPLOYER) {
      return res.status(400).json({
        status: false,
        message: "EMPLOYER cannot apply on a job!",
      });
    }

    // check if job exists
    const job = await Job.findOne({
      where: {
        id: jobId,
      },
    });
    if (!job) {
      return res.status(400).json({
        status: false,
        message: "Job does not exist!",
      });
    }

    // check for userId and jobId combination i.e; if user has already applied on a job
    const jobApplication = await UserJob.findOne({
      where: {
        userId,
        jobId,
      },
    });
    if (jobApplication) {
      return res.status(400).json({
        status: false,
        message: "Already applied on this job!",
      });
    }

    // check for lastDate to apply and today
    if (new Date() > new Date(job.lastDate)) {
      return res.status(400).json({
        status: false,
        message: "Cannot apply after last date!",
      });
    }

    // start transaction
    // save userId and jobId
    await UserJob.create({
      userId,
      jobId,
    });

    // success
    return res.json({
      status: true,
      message: "Job application submitted successfully!",
    });
  } catch (error) {
    // error
    console.log("Error while applying for the job: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

const deleteJob = async (req, res) => {
  try {
    // destructure required data
    const { id } = req.params;
    const { id: userId } = req.user;

    // check if job exists for the user
    const userWithJob = await User.findOne({
      where: {
        id: userId,
        type: EMPLOYER,
      },
      attributes: {
        include: [],
      },
      include: {
        model: Job,
        as: "userJobs",
        where: {
          id,
        },
      },
    });

    if (!userWithJob || !userWithJob.userJobs.length) {
      return res.status(400).json({
        status: true,
        message: "Employer doesn't own this job!",
      });
    }

    // delete the job
    await Job.destroy({
      where: {
        id,
      },
    });

    // success
    return res.json({
      status: true,
      message: DATA_DELETION_MESSAGE,
    });
  } catch (error) {
    // error
    console.log("Error while deleting the job: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

module.exports = {
  createJob,
  getJobs,
  getJob,
  applyJob,
  updateJob,
  deleteJob,
};
