require("dotenv").config();

const { Category } = require("../db/models");

const {
  responseMessages: { SERVER_ERROR_MESSAGE },
} = require("../constants");

// eslint-disable-next-line consistent-return
const getCategories = async (req, res) => {
  try {
    // get categories
    const data = await Category.findAll({});

    // success
    return res.json({
      status: true,
      data,
    });
  } catch (error) {
    // error
    console.log("Error while fetching categories: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

module.exports = {
  getCategories,
};
