const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
require("dotenv").config();

const { User } = require("../db/models");
// const { sendEmail } = require("../helpers");
const {
  responseMessages: { SERVER_ERROR_MESSAGE },
} = require("../constants");

// eslint-disable-next-line consistent-return
const login = async (req, res) => {
  try {
    // destructure request body
    const { email, password } = req.body;

    // find user with the email
    const data = await User.findOne({
      where: {
        email,
      },
    });

    if (!data) {
      return res.status(404).json({
        status: false,
        message: "Please enter correct email!",
      });
    }

    // compare passwords
    const isMatched = await bcrypt.compare(password, data.password);

    if (!isMatched) {
      return res.status(403).json({
        status: false,
        message: "Please enter correct password!",
      });
    }

    // delete password from result data
    delete data.dataValues.password;

    // generate jwt
    const token = jwt.sign(
      { id: data.dataValues.id, email, type: data.dataValues.type },
      process.env.JWT_SECRET_KEY,
      {
        expiresIn: "1h",
      }
    );

    // send login email
    // await sendEmail(email);

    // success
    return res.status(200).json({
      status: true,
      message: "Logged in successfully!",
      data,
      token,
    });
  } catch (error) {
    // error
    console.log("Error while login: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

module.exports = {
  login,
};
