require("dotenv").config();

const { User, Job, Interview } = require("../db/models");
const {
  userTypes: { EMPLOYER, JOBSEEKER },
  responseMessages: {
    DATA_CREATION_MESSAGE,
    DATA_UPDATION_MESSAGE,
    DATA_DELETION_MESSAGE,
  },
} = require("../constants");
const { sendEmail } = require("../helpers");

const {
  responseMessages: { SERVER_ERROR_MESSAGE },
} = require("../constants");

// eslint-disable-next-line consistent-return
const scheduleInterview = async (req, res) => {
  try {
    // destructure required data
    const { id: employerId, type } = req.user;
    const { jobSeekerId, jobId, date } = req.body;

    // check if user is an employer
    if (type !== EMPLOYER) {
      return res.status(400).json({
        status: false,
        message: "Only employer can schedule the interview!",
      });
    }

    // check if job seeker exists
    const jobSeeker = await User.findOne({
      where: {
        id: jobSeekerId,
        type: JOBSEEKER,
      },
    });
    if (!jobSeeker) {
      return res.status(400).json({
        status: false,
        message: "JobSeeker doesn't exist!",
      });
    }

    // check if job exists
    const job = await Job.findOne({
      where: {
        id: jobId,
      },
    });
    if (!job) {
      return res.status(400).json({
        status: false,
        message: "Job doesn't exist!",
      });
    }

    // check if interview already exists
    const interview = await Interview.findOne({
      where: {
        employerId,
        jobSeekerId,
        jobId,
      },
    });
    console.log("interview: ", interview);

    if (interview) {
      return res.status(400).json({
        status: false,
        message: "Data already exists!",
      });
    }

    // create the interview
    const data = await Interview.create({
      employerId,
      jobSeekerId,
      jobId,
      date,
    });

    // send interview email
    await sendEmail({
      to: jobSeeker.email,
      subject: "Interview Schedule - Job Portal",
      text: `You have been invited for an interview by Job Portal on ${data.createdAt}`,
      html: `<strong>Interview scheduled at ${date}</strong> `,
    });

    // success
    return res.status(201).json({
      status: true,
      message: DATA_CREATION_MESSAGE,
    });
  } catch (error) {
    // error
    console.log("Error while shceduling interview: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

const getUserSpecificInterviews = async (req, res) => {
  try {
    // destructure required data
    const { id, type } = req.user;

    const queryFilter = {};

    if (type === EMPLOYER) {
      queryFilter.employerId = id;
    } else {
      queryFilter.jobSeekerId = id;
    }

    // fetch user interviews
    const data = await Interview.findAll({
      where: queryFilter,
      include: [
        {
          model: User,
          as: type === EMPLOYER ? "interviewEmployers" : "interviewJobSeekers",
        },
        {
          model: Job,
          as: "interviewJobs",
        },
      ],
    });

    return res.json({
      status: true,
      data,
    });
  } catch (error) {
    // error
    console.log("Error while fetching interviews: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

const rescheduleInterview = async (req, res) => {
  try {
    // destructure required data
    const { id } = req.params;
    const { date } = req.body;
    const { id: userId, type } = req.user;

    // check if user is an employer
    if (type !== EMPLOYER) {
      return res.status(403).json({
        status: false,
        message: "Only employer can reschedule the interview!",
      });
    }

    // check if interview exists
    const interview = await Interview.findOne({
      where: {
        id,
        employerId: userId,
      },
    });
    if (!interview) {
      return res.status(400).json({
        status: false,
        message: "Data doesn't exist!",
      });
    }

    // update the interview
    await Interview.update(
      { date: new Date(date) },
      {
        where: {
          id,
        },
      }
    );

    // get the updated interview
    const data = await Interview.findOne({
      where: {
        id,
      },
    });

    // success
    return res.json({
      status: true,
      message: DATA_UPDATION_MESSAGE,
      data,
    });
  } catch (error) {
    // error
    console.log("Error while rescheduling the interview: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

const deleteInterview = async (req, res) => {
  try {
    // destructure required data
    const { id } = req.params;
    const { id: userId, type } = req.user;

    if (type !== EMPLOYER) {
      return res.status(403).json({
        status: false,
        message: "Only employer can delete an interview!",
      });
    }

    // check if interview exists for the employer
    const interview = await Interview.findOne({
      where: {
        id,
        employerId: userId,
      },
      include: {
        model: User,
        as: "interviewJobSeekers",
      },
    });
    if (!interview) {
      return res.status(404).json({
        status: false,
        message: "Interview doesn't exist!",
      });
    }

    // delete the interview
    await Interview.destroy({
      where: {
        id,
      },
    });

    // send interview removal email
    await sendEmail({
      to: interview.interviewJobSeekers.email,
      subject: "Interview Schedule - Job Portal",
      text: `Your interview has been removed by Job Portal on ${new Date()}`,
      html: `<strong>You will be informed about the reschedule soon.</strong> `,
    });

    // success
    return res.json({
      status: true,
      message: DATA_DELETION_MESSAGE,
      data: interview,
    });
  } catch (error) {
    // error
    console.log("Error while rescheduling the interview: ", error);
    return res.status(500).json({
      status: false,
      message: SERVER_ERROR_MESSAGE,
    });
  }
};

module.exports = {
  scheduleInterview,
  getUserSpecificInterviews,
  rescheduleInterview,
  deleteInterview,
};
