const router = require("express").Router();
const { validate } = require("express-validation");

const {
  createUserValidation,
  idParameterValidation,
  userTypeParamValidation,
  updateUserValidation,
} = require("../validation");
const { verifyToken, upload } = require("../middlewares");
const {
  createUser,
  updateUser,
  getUsersByType,
  getUser,
  deleteUser,
} = require("../controllers");

router.post(
  "/",
  upload.single("resume"),
  validate(createUserValidation),
  createUser
);
router.patch(
  "/:id",
  upload.single("resume"),
  validate(updateUserValidation),
  updateUser
);
router.get("/", validate(userTypeParamValidation), getUsersByType);
router.get("/:id", validate(idParameterValidation), getUser);
router.delete("/:id", verifyToken, validate(idParameterValidation), deleteUser);

module.exports = router;
