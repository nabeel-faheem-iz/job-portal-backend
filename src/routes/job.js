const router = require("express").Router();
const { validate } = require("express-validation");

const { verifyToken } = require("../middlewares");
const {
  createJobValidation,
  searchJobValidation,
  applyJobValidation,
  updateJobValidation,
  idParameterValidation,
} = require("../validation");
const {
  createJob,
  updateJob,
  getJobs,
  getJob,
  applyJob,
  deleteJob,
} = require("../controllers");

router.post("/", verifyToken, validate(createJobValidation), createJob);
router.post(
  "/application",
  verifyToken,
  validate(applyJobValidation),
  applyJob
);
router.get("/", validate(searchJobValidation), getJobs);
router.get("/:id", validate(idParameterValidation), getJob);
router.patch("/:id", verifyToken, validate(updateJobValidation), updateJob);
router.delete("/:id", verifyToken, validate(idParameterValidation), deleteJob);

module.exports = router;
