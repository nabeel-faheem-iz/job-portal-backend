const router = require("express").Router();
const { validate } = require("express-validation");

const {
  scheduleInterviewValidation,
  idParameterValidation,
} = require("../validation");
const { verifyToken } = require("../middlewares");
const {
  scheduleInterview,
  getUserSpecificInterviews,
  rescheduleInterview,
  deleteInterview,
} = require("../controllers");

router.post(
  "/",
  verifyToken,
  validate(scheduleInterviewValidation),
  scheduleInterview
);
router.get("/", verifyToken, getUserSpecificInterviews);
router.patch("/:id", verifyToken, rescheduleInterview);
router.delete(
  "/:id",
  verifyToken,
  validate(idParameterValidation),
  deleteInterview
);

module.exports = router;
