const router = require("express").Router();
const { validate } = require("express-validation");

const { authValidation } = require("../validation");
const { login } = require("../controllers");

router.post("/", validate(authValidation), login);

module.exports = router;
