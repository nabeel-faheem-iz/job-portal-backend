const router = require("express").Router();

const userRoutes = require("./user");
const authRoutes = require("./auth");
const jobRoutes = require("./job");
const interviewRoutes = require("./interview");
const categoryRoutes = require("./category");

router.use("/auth", authRoutes);
router.use("/users", userRoutes);
router.use("/jobs", jobRoutes);
router.use("/interviews", interviewRoutes);
router.use("/categories", categoryRoutes);

module.exports = router;
