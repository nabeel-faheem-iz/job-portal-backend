const express = require("express");
const { ValidationError } = require("express-validation");
const { MulterError } = require("multer");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./docs/swagger.json");
require("dotenv").config();

const routes = require("./routes");

const app = express();

app.use(express.json());

// mount app routes
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use("/api", routes);

// handle validation errors
// eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => {
  console.log("inside global error handler: ", error);
  if (error instanceof ValidationError || error instanceof MulterError) {
    return res.status(error.statusCode).json(error);
  }

  return res.status(500).json(error);
});

app.listen(process.env.PORT, async () => {
  console.log(`Server started at ${process.env.PORT} 🛩️`);
});
