const nodemailer = require("nodemailer");

async function sendEmail({ to, subject, text, html }) {
  console.log(`Sending email to ${to}`);

  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.EMAIL_SENDER,
      pass: process.env.EMAIL_SENDER_PASSWORD,
    },
  });

  const info = await transporter.sendMail({
    from: process.env.EMAIL_SENDER,
    to,
    subject,
    text,
    html,
  });

  console.log("Email sent!!!", info.messageId);
}

module.exports = sendEmail;
