const hashPassword = require("./hashPassword");
const sendEmail = require("./sendEmail");

module.exports = {
  hashPassword,
  sendEmail,
};
