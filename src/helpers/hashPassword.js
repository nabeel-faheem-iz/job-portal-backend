const bcrypt = require("bcrypt");
require("dotenv").config();

function hashPassword(password) {
  return bcrypt.hashSync(
    password,
    bcrypt.genSaltSync(parseInt(process.env.SALT_ROUNDS, 10))
  );
}

module.exports = hashPassword;
