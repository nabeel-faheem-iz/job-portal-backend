const verifyToken = require("./auth");
const upload = require("./upload");

module.exports = {
  verifyToken,
  upload,
};
