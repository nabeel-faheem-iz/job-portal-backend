const jwt = require("jsonwebtoken");
require("dotenv").config();

// eslint-disable-next-line consistent-return
function verifyToken(req, res, next) {
  const header = req.headers.authorization;

  const token = header && header.split(" ")[1];

  if (token == null) return res.sendStatus(401);

  // eslint-disable-next-line consistent-return
  const data = jwt.verify(token, process.env.JWT_SECRET_KEY);

  if (!data) return res.sendStatus(403);

  req.user = data;

  next();
}

module.exports = verifyToken;
