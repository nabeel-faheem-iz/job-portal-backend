const { Joi } = require("express-validation");

const scheduleInterviewValidation = {
  body: Joi.object({
    jobSeekerId: Joi.number().required(),
    jobId: Joi.number().required(),
    date: Joi.date().required(),
  }),
};

const rescheduleInterviewValidation = {
  params: Joi.object({
    id: Joi.string().required(),
  }),
  body: Joi.object({
    date: Joi.date().required(),
  }),
};

module.exports = {
  scheduleInterviewValidation,
  rescheduleInterviewValidation,
};
