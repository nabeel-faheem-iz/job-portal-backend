const { Joi } = require("express-validation");

const idParameterValidation = {
  params: Joi.object({
    id: Joi.string().required(),
  }),
};

module.exports = {
  idParameterValidation,
};
