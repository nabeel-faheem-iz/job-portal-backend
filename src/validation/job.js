const { Joi } = require("express-validation");

const createJobValidation = {
  body: Joi.object({
    name: Joi.string().max(80).required(),
    positions: Joi.number().required(),
    lastDate: Joi.date().required(),
  }),
};

const searchJobValidation = {
  query: Joi.object({
    name: Joi.string(),
    category: Joi.string(),
  }),
};

const applyJobValidation = {
  body: Joi.object({
    jobId: Joi.number().required(),
  }),
};

const updateJobValidation = {
  params: Joi.object({
    id: Joi.string().required(),
  }),
  body: Joi.object({
    name: Joi.string().max(80),
    positions: Joi.number(),
    lastDate: Joi.date(),
  }),
};

module.exports = {
  createJobValidation,
  searchJobValidation,
  applyJobValidation,
  updateJobValidation,
};
