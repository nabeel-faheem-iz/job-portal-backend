const { Joi } = require("express-validation");

const authValidation = {
  body: Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  }),
};

module.exports = {
  authValidation,
};
