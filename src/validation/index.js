const { idParameterValidation } = require("./common");
const {
  createUserValidation,
  userTypeParamValidation,
  updateUserValidation,
} = require("./user");
const { authValidation } = require("./auth");
const {
  createJobValidation,
  searchJobValidation,
  applyJobValidation,
  updateJobValidation,
} = require("./job");
const { scheduleInterviewValidation } = require("./interview");

module.exports = {
  authValidation,
  idParameterValidation,
  createUserValidation,
  userTypeParamValidation,
  updateUserValidation,
  createJobValidation,
  searchJobValidation,
  applyJobValidation,
  updateJobValidation,
  scheduleInterviewValidation,
};
