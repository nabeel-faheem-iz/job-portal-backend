const { Joi } = require("express-validation");

const {
  userTypes: { EMPLOYER, JOBSEEKER },
} = require("../constants");

const createUserValidation = {
  body: Joi.object({
    firstName: Joi.string().min(3).max(50).required(),
    lastName: Joi.string().min(3).max(50).required(),
    phone: Joi.string()
      .length(11)
      // .pattern(/^[0-9]$/)
      .required(),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    type: Joi.string().valid(EMPLOYER, JOBSEEKER).required(),
  }),
};

const userTypeParamValidation = {
  query: Joi.object({
    type: Joi.string().valid("employers", "jobseekers", "").required(),
  }),
};

const updateUserValidation = {
  body: Joi.object({
    firstName: Joi.string().min(3).max(50),
    lastName: Joi.string().min(3).max(50),
    phone: Joi.string().length(11),
    password: Joi.string(),
  }),
  params: Joi.object({
    id: Joi.string().required(),
  }),
};

module.exports = {
  createUserValidation,
  userTypeParamValidation,
  updateUserValidation,
};
