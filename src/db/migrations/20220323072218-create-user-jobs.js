module.exports = {
  async up(queryInterface, { INTEGER, DATE }) {
    await queryInterface.createTable("UserJobs", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: INTEGER,
      },
      jobId: {
        type: INTEGER,
        allowNull: false,
        onDelete: "CASCADE",
        references: {
          model: "Jobs",
          key: "id",
          as: "jobId",
        },
      },
      userId: {
        type: INTEGER,
        onDelete: "CASCADE",
        references: {
          model: "Users",
          key: "id",
          as: "userId",
        },
      },
      createdAt: {
        allowNull: false,
        type: DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DATE,
      },
      deletedAt: {
        type: DATE,
      },
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable("UserJobs");
  },
};
