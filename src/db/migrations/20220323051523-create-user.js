module.exports = {
  async up(queryInterface, { INTEGER, STRING, ENUM, DATE }) {
    await queryInterface.createTable("Users", {
      id: {
        type: INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      firstName: {
        type: STRING,
        allowNull: false,
        validate: {
          min: 3,
          max: 50,
        },
      },
      lastName: {
        type: STRING,
        allowNull: false,
        validate: {
          min: 3,
          max: 50,
        },
      },
      phone: {
        type: STRING(11),
        allowNull: false,
        unique: true,
        validate: {
          // is: /^[0-9]+$/i,
        },
      },
      email: {
        type: STRING(50),
        allowNull: false,
        unique: true,
        validate: {
          isEmail: true,
        },
      },
      password: {
        type: STRING,
        allowNull: false,
      },
      type: {
        type: ENUM(["EMPLOYER", "JOBSEEKER"]),
        allowNull: false,
      },
      resume: {
        type: STRING,
        // null for EMPLOYER
      },
      createdAt: {
        type: DATE,
        allowNull: false,
      },
      updatedAt: {
        type: DATE,
        allowNull: false,
      },
      deletedAt: {
        type: DATE,
      },
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable("Users");
  },
};
