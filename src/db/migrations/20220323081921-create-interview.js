module.exports = {
  async up(queryInterface, { INTEGER, DATE }) {
    await queryInterface.createTable("Interviews", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: INTEGER,
      },
      // jobSeekerId: {
      //   type: INTEGER,
      //   allowNull: false,
      //   references: {
      //     model: "Users",
      //     key: "id",
      //     as: "jobSeekerId",
      //   },
      // },
      // employerId: {
      //   type: INTEGER,
      //   allowNull: false,
      //   references: {
      //     model: "Users",
      //     key: "id",
      //     as: "employerId",
      //   },
      // },
      jobSeekerId: {
        type: INTEGER,
        allowNull: false,
        references: {
          model: "Users",
          key: "id",
          as: "jobSeekerId",
        },
      },
      employerId: {
        type: INTEGER,
        allowNull: false,
        references: {
          model: "Users",
          key: "id",
          as: "employerId",
        },
      },
      jobId: {
        type: INTEGER,
        allowNull: false,
        references: {
          model: "Jobs",
          key: "id",
          as: "jobId",
        },
      },
      date: {
        type: DATE,
        allowNull: false,
      },
      createdAt: {
        type: DATE,
        allowNull: false,
      },
      updatedAt: {
        type: DATE,
        allowNull: false,
      },
      deletedAt: {
        type: DATE,
      },
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable("Interviews");
  },
};
