module.exports = {
  async up(queryInterface, { INTEGER, DATE }) {
    await queryInterface.createTable("CategoryJobs", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: INTEGER,
      },
      jobId: {
        type: INTEGER,
        allowNull: false,
        references: {
          model: "Jobs",
          key: "id",
          as: "jobId",
        },
      },
      categoryId: {
        type: INTEGER,
        allowNull: false,
        references: {
          model: "Categories",
          key: "id",
          as: "categoryId",
        },
      },
      createdAt: {
        allowNull: false,
        type: DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DATE,
      },
      deletedAt: {
        type: DATE,
      },
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable("CategoryJobs");
  },
};
