module.exports = {
  async up(queryInterface, { INTEGER, STRING, DATE }) {
    await queryInterface.createTable("Jobs", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: INTEGER,
      },
      name: {
        type: STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          max: 80,
        },
      },
      positions: {
        type: INTEGER,
        allowNull: false,
      },
      lastDate: {
        type: DATE,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DATE,
      },
      deletedAt: {
        type: DATE,
      },
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable("Jobs");
  },
};
