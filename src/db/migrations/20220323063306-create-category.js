module.exports = {
  async up(queryInterface, { INTEGER, STRING, DATE }) {
    await queryInterface.createTable("Categories", {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: STRING,
        allowNull: false,
        validate: {
          min: 2,
          max: 50,
          notEmpty: true,
        },
      },
      createdAt: {
        type: DATE,
        allowNull: false,
      },
      updatedAt: {
        type: DATE,
        allowNull: false,
      },
      deletedAt: {
        type: DATE,
      },
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable("Categories");
  },
};
