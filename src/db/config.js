require("dotenv").config();

module.exports = {
  development: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB,
    host: process.env.HOST,
    dialect: process.env.DIALECT,
    seederStorage: "sequelize",
    // Use a different table name. Default: SequelizeData
    // seederStorageTableName: "sequelize_data"
  },
  test: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB,
    host: process.env.HOST,
    dialect: process.env.DIALECT,
    seederStorage: "sequelize",
    // Use a different table name. Default: SequelizeData
    // seederStorageTableName: "sequelize_data"
  },
  production: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB,
    host: process.env.HOST,
    dialect: process.env.DIALECT,
    seederStorage: "sequelize",
    // Use a different table name. Default: SequelizeData
    // seederStorageTableName: "sequelize_data"
  },
};
