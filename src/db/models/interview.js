const { Model } = require("sequelize");

module.exports = (sequelize, { INTEGER, DATE }) => {
  class Interview extends Model {
    static associate({ User, Job }) {
      this.belongsTo(User, {
        foreignKey: "jobSeekerId",
        as: "interviewJobSeekers",
        onDelete: "CASCADE",
      });
      this.belongsTo(User, {
        foreignKey: "employerId",
        as: "interviewEmployers",
        onDelete: "CASCADE",
      });
      this.belongsTo(Job, {
        foreignKey: "jobId",
        as: "interviewJobs",
        onDelete: "CASCADE",
      });
    }
  }
  Interview.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: INTEGER,
      },
      jobSeekerId: {
        type: INTEGER,
        allowNull: false,
        references: {
          model: "Users",
          key: "id",
          as: "jobSeekerId",
        },
      },
      employerId: {
        type: INTEGER,
        allowNull: false,
        references: {
          model: "Users",
          key: "id",
          as: "employerId",
        },
      },
      jobId: {
        type: INTEGER,
        allowNull: false,
        references: {
          model: "Jobs",
          key: "id",
        },
      },
      date: {
        type: DATE,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Interview",
      paranoid: true,
    }
  );
  return Interview;
};
