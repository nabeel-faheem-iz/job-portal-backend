const { Model } = require("sequelize");

module.exports = (sequelize, { INTEGER, STRING, ENUM }) => {
  class User extends Model {
    static associate({ Job, Interview }) {
      this.belongsToMany(Job, {
        foreignKey: "userId",
        through: "UserJob",
        as: "userJobs",
      });
      // this.belongsToMany(Job, {
      //   foreignKey: "jobSeekerId",
      //   through: "Interview",
      //   as: "jobSeekerInterviews",
      // });
      // this.belongsToMany(Job, {
      //   foreignKey: "employerId",
      //   through: "Interview",
      //   as: "employerInterviews",
      // });
      this.hasMany(Interview, {
        foreignKey: "jobSeekerId",
        as: "jobSeekerInterviews",
      });
      this.hasMany(Interview, {
        foreignKey: "employerId",
        as: "employerInterviews",
      });
    }
  }
  User.init(
    {
      id: {
        type: INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      firstName: {
        type: STRING,
        allowNull: false,
        validate: {
          min: 3,
          max: 50,
        },
      },
      lastName: {
        type: STRING,
        allowNull: false,
        validate: {
          min: 3,
          max: 50,
        },
      },
      phone: {
        type: STRING(11),
        allowNull: false,
        unique: true,
        validate: {
          // is: /^[0-9]+$/,
        },
      },
      email: {
        type: STRING(50),
        allowNull: false,
        unique: true,
        validate: {
          isEmail: true,
        },
      },
      password: {
        type: STRING,
        allowNull: false,
      },
      type: {
        type: ENUM(["EMPLOYER", "JOBSEEKER"]),
        allowNull: false,
      },
      resume: {
        type: STRING,
        // null for EMPLOYER
      },
    },
    {
      sequelize,
      modelName: "User",
      paranoid: true,
    }
  );
  return User;
};
