const { Model } = require("sequelize");

module.exports = (sequelize, { INTEGER, STRING }) => {
  class Category extends Model {
    static associate({ Job }) {
      this.belongsToMany(Job, {
        foreignKey: "categoryId",
        through: "CategoryJob",
        as: "jobs",
      });
    }
  }
  Category.init(
    {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: STRING,
        allowNull: false,
        validate: {
          min: 2,
          max: 50,
          notEmpty: true,
        },
      },
    },
    {
      sequelize,
      modelName: "Category",
      paranoid: true,
    }
  );
  return Category;
};
