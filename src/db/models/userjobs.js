const { Model } = require("sequelize");

module.exports = (sequelize, { INTEGER }) => {
  class UserJob extends Model {
    static associate() {}
  }
  UserJob.init(
    {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      jobId: {
        type: INTEGER,
        allowNull: false,
        references: {
          model: "Jobs",
          key: "id",
        },
      },
      userId: {
        type: INTEGER, // null in case of user is an "EMPLOYER"
        references: {
          model: "Users",
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "UserJob",
      paranoid: true,
    }
  );
  return UserJob;
};
