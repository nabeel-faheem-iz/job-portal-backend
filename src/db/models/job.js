const { Model } = require("sequelize");

module.exports = (sequelize, { INTEGER, STRING, DATE }) => {
  class Job extends Model {
    static associate({ User, Interview, Category }) {
      this.belongsToMany(User, {
        foreignKey: "jobId",
        through: "UserJob",
        as: "jobUsers",
      });
      // this.belongsToMany(User, {
      //   foreignKey: "jobId",
      //   through: "Interview",
      //   as: "interviewUsers",
      // });
      this.hasMany(Interview, {
        foreignKey: "jobId",
        as: "jobInterviews",
      });
      this.belongsToMany(Category, {
        foreignKey: "jobId",
        through: "CategoryJob",
        as: "categories",
      });
    }
  }
  Job.init(
    {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      name: {
        type: STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          max: 80,
        },
      },
      positions: {
        type: INTEGER,
        allowNull: false,
      },
      lastDate: {
        type: DATE,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Job",
      paranoid: true,
    }
  );
  return Job;
};
