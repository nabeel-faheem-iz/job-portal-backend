const { Model } = require("sequelize");

module.exports = (sequelize, { INTEGER }) => {
  class CategoryJob extends Model {
    static associate() {
      // define association here
    }
  }
  CategoryJob.init(
    {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      jobId: {
        type: INTEGER,
        allowNull: false,
      },
      categoryId: {
        type: INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "CategoryJob",
      paranoid: true,
    }
  );
  return CategoryJob;
};
