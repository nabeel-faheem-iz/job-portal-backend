## Job Portal Application
JOB Portal NodeJS application with Express, SequelizeORM, PostgresSQL.

## Name
Job Portal Application

## Description
It is a Job Portal application built on top of NodeJS, Express, SequelizeORM and PostgresSQL. User authentication supported!

## Installation
To make project up and running, go to the root of your project and run this command in your terminal:
npm install

## License
For open source projects, say how it is licensed.